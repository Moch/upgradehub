import { useState } from "react";
import './App.css';
import {BrowserRouter as Router } from "react-router-dom";

import { Menu } from "./core/components/Menu/Menu";
import { Routes } from "./core/components/Routes/Routes";
import { Loading, LoadingContext } from './core/components/Loading/Loading';

function App() {

  const [isLoading, setIsLoading] = useState(false);

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <div className="container">
            <Menu></Menu>
            <LoadingContext.Provider value={{isLoading, setIsLoading}}>
              <Loading></Loading>
              <Routes></Routes>
            </LoadingContext.Provider>
          </div>
        </header>
      </div>
    </Router>
  );
}

export default App;
