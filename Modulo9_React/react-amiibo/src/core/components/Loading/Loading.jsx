import React, {createContext, useContext} from "react";

import "./Loading.scss";



export const LoadingContext = createContext()

export function Loading() {

  const {isLoading} = useContext(LoadingContext);

  return (
    isLoading && <div className="c-loading">
      <div className="lds-hourglass"></div>
    </div>
  )
}

