import React from "react";
import { NavLink } from "react-router-dom";


export function Menu(props) {

  return (
    <nav>
      <NavLink to="/">Home</NavLink>
      <NavLink to="/amiibos">Amiibos</NavLink>
      <NavLink to="/gameseries">Game Series</NavLink>
      <NavLink to="/contact">Contact</NavLink>
    </nav>
  )

}