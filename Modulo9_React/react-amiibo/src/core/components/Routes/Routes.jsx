import React from "react";
import { Route, Switch } from "react-router-dom";

 
import { AmiibosPage } from "../../../pages/AmiibosPage/AmiibosPage";
import { AmiiboDetailPage } from "../../../pages/AmiiboDetailPage/AmiiboDetailPage";
import { GameSeriesPage } from "../../../pages/GamesSeriesPage/GameSeriesPage";
import { HomePage } from "../../../pages/HomePage/HomePage";
import { ContactPage } from "../../../pages/ContactPage/ContactPage";


export function Routes(props) {

  return (
    <Switch>
      <Route path="/amiibos/:tail">
        <AmiiboDetailPage></AmiiboDetailPage>
      </Route>
      <Route path="/amiibos">
        <AmiibosPage></AmiibosPage>
      </Route>
      <Route path="/gameseries">
        <GameSeriesPage></GameSeriesPage>
      </Route>
      <Route path="/contact">
        <ContactPage></ContactPage>
      </Route>
      <Route path="/">
        <HomePage></HomePage>
      </Route>
    </Switch>
  )

}