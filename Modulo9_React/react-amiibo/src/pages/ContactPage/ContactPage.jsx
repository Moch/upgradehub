import React from "react";
import {useForm} from "react-hook-form";


export function ContactPage() {

  const { register, handleSubmit, watch, errors, reset } = useForm();

  const doSubmit = (data) => {
    console.log(data);
    reset();
  }

  console.log(watch("name"));

  return (
    <form onSubmit={handleSubmit(doSubmit)}>
      <label>Name
        <input 
          type="text" 
          name="name" 
          ref={register({required: true, minLength: 4, maxLength: 10})}/>
      </label>
      {errors.name && <span>Name is required and needs to be more than 4 and less than 10</span>}

      <label>Surname
        <input 
          type="text" 
          name="surname" 
          ref={register({required: true, minLength: 4})}/>
      </label>
      {errors.surname && <span>Surname is required and needs to be more than 4</span>}

      <input type="submit" value="Send"/>
    </form>
  )
}