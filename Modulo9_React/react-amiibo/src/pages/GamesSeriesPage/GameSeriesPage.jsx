import React, { useEffect, useState } from "react";

import { API } from "../../shared/consts/api.consts";
import { GameSeriesGallery } from "./GameSeriesGallery/GameSeriesGallery";


export function GameSeriesPage(props) {

  const [gameSeries, setGameSeries] = useState([]);

  const getGameSeries = () => {
    API.get("gameseries").then( (res) => {
      setGameSeries(res.data.amiibo);
    });
  };

  useEffect(getGameSeries, []);

  return (
    <div>
      <h1>GAME SERIES PAGE</h1>
      <GameSeriesGallery gameSeries={gameSeries}></GameSeriesGallery>
    </div>
  )

}