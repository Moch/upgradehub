

import "./GameSeriesGallery.scss";

export function GameSeriesGallery(props) {

  return (
    <div className="GameSeriesGallery">
      {props.gameSeries.map( (g,i) => 
        <h5 key={i} className="GameSeriesGallery__card">{g.name}</h5>
      )}
    </div>
  )

}