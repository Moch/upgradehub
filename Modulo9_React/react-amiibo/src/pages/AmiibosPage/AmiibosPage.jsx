import React, { useContext, useEffect, useState } from "react";

import { LoadingContext } from "../../core/components/Loading/Loading";
import { API } from "../../shared/consts/api.consts";
import { AmiibosGallery } from "./AmiibosGallery/AmiibosGallery";


export function AmiibosPage(props) {

  const [counter, setCounter] = useState(0);

  const [amiibos, setAmiibos] = useState([]);

  const { setIsLoading } = useContext(LoadingContext)

  const consoleMe = () => {
    console.log("Hola");
  }

  useEffect(consoleMe, []);
  useEffect(consoleMe, [counter]);

  const getAmiibos = () => {
    setIsLoading(true);
    API.get("amiibo").then( (res) => {
      setIsLoading(false);
      setAmiibos(res.data.amiibo);
    });
  };

  useEffect(getAmiibos, []);

  return (
    <div>
      <h1>AMIIBOS PAGE</h1>
      {counter}
      <button onClick={() => setCounter(counter + 1)}>+</button>
      <AmiibosGallery amiibos={amiibos}></AmiibosGallery>
    </div>
  )

}