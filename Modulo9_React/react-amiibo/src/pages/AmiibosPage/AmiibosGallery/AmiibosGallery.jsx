
import { Link } from "react-router-dom";

import "./AmiibosGallery.scss";

export function AmiibosGallery(props) {

  return (
    <div className="amiibos">
      {props.amiibos.map( (a,i) => 
        <Link to={"/amiibos/" + a.tail} className="amiibos__link">
          <figure key={i} className="amiibos__figure">
            <img alt={a.name} src={a.image}/>
            <h5>{a.name}</h5>
          </figure>
        </Link>
      )}
    </div>
  )

}