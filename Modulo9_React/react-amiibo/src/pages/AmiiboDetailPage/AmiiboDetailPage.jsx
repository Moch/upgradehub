import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { API } from "../../shared/consts/api.consts";
import "./AmiiboDetailPage.scss";



export function AmiiboDetailPage(props) {

  const [amiibo, setAmiibo] = useState(null);

  const { tail } = useParams();

  const getAmiibo = () => {
    API.get("amiibo?tail=" + tail).then( (res) => {
      setAmiibo(res.data.amiibo[0]);
    });
  };

  useEffect(getAmiibo, []);

  return (
    <div>
      <h1>AMIIBO DETAIL PAGE</h1>
      {amiibo && <figure className="amiiboDetail">
        <img alt={amiibo.name} src={amiibo.image}/>
        <div className="amiiboDetail__line">
          <span>character:</span>
          <strong>{amiibo.character}</strong>
        </div>
        <div className="amiiboDetail__line">
          <span>name:</span>
          <strong>{amiibo.name}</strong>
        </div>
        <div className="amiiboDetail__line">
          <span>amiibo series:</span>
          <strong>{amiibo.amiiboSeries}</strong>
        </div>
        <div className="amiiboDetail__line">
          <span>game series:</span>
          <strong>{amiibo.gameSeries}</strong>
        </div>
        <div className="amiiboDetail__line">
          <span>type:</span>
          <strong>{amiibo.type}</strong>
        </div>
        <div className="amiiboDetail__line">
          <span>release:</span>
          <ul>
            <li>au: {amiibo.release.au}</li>
            <li>eu: {amiibo.release.eu}</li>
            <li>jp: {amiibo.release.jp}</li>
            <li>na: {amiibo.release.na}</li>
          </ul>
        </div>
        </figure>}
    </div>
  )

}