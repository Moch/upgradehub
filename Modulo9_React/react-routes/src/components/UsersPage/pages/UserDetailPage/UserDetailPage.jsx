import React from "react";
import { useHistory, useParams } from "react-router-dom";

export function UserDetailPage(props) {

  const { userName } = useParams();

  const history = useHistory();

  return (
    <h1>{userName}</h1>
  );
}