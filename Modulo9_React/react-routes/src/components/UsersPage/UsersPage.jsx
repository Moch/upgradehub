import React from "react";
import { Link } from "react-router-dom";

export function UsersPage(props) {

  const users = [
    "Abel",
    "Cristóbal",
    "Mercedes",
    "Estefanía",
    "Ramiro"
  ]

  return (
    <ul>
      {users.map((user, i) => <li key={i}>
        <Link to={"/users/" + user}>{user}</Link>
      </li>)}
    </ul>
  );
}