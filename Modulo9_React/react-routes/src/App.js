import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { ContactPage } from './components/ContactPage/ContactPage';
import { Home } from './components/Home/Home';
import { UsersPage } from './components/UsersPage/UsersPage';
import { UserDetailPage } from './components/UsersPage/pages/UserDetailPage/UserDetailPage';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Switch>
            <Route path="/contact">
              <ContactPage></ContactPage>
            </Route>
            <Route path="/users/:userName">
              <UserDetailPage></UserDetailPage>
            </Route>
            <Route path="/users">
              <UsersPage></UsersPage>
            </Route>
            <Route path="/">
              <Home></Home>
            </Route>
          </Switch>
        </header>
      </div>
    </Router>
  );
}

export default App;
