import React from "react";


export class ButtonAlert extends React.Component {

  alertMe = () => {
    alert("Hello World");
  }

  alertMeWithParams = (text) => {
    alert(text)
  }

  render() {
    return(
      <div>
        <button onClick={this.alertMe}>Alert me!</button>
        <button onClick={() => {this.alertMeWithParams("Hola")}}>Alert me 2</button>
      </div>
    )
  }
}