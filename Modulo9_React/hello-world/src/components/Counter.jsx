import React from "react";


export class Counter extends React.Component {

  state = { count: 0 };

  add = () => {
    // this.state.count = this.state.count + 1   ESTO NO FUNCIONA PORQUE EL ESTADO ES INMUTABLE
    this.setState({ count: this.state.count + 1 });
  }

  operate = (s) => {
    switch (s) {
      case "+":
        this.setState({ count: this.state.count + 1 });
        break;
      case "-":
        this.setState({ count: this.state.count - 1 });
        break;
      case "*2":
        this.setState({ count: this.state.count * 2 });
        break;
      case "/2":
        this.setState({ count: this.state.count / 2 });
        break
      default: 
        this.setState({ count: 0 });
        break;
    }
  } 

  render() {
    return(
      <div>
        <h2>The count is {this.state.count}</h2>
        <button onClick={() => {this.operate("+")}}>+</button>
        <button onClick={() => {this.operate("-")}}>-</button>
        <button onClick={() => {this.operate("*2")}}>*2</button>
        <button onClick={() => {this.operate("/2")}}>/2</button>
        <button onClick={() => {this.operate("")}}>reset</button>
      </div>
    )
  }
}