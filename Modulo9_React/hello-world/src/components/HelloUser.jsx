import React from "react";


export class HelloUser extends React.Component {
  render() {
    const user = {name: "Rick", age: 45};
    return(
      <h1>Hello {user.name} you have {user.age} years old</h1>
    )
  }
}