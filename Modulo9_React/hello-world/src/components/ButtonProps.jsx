import React from "react";


export class ButtonProps extends React.Component {
  render() {
    return(
      <button>{this.props.text}</button>
    )
  }
}