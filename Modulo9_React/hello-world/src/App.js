
import './App.css';

import {Card} from "./components/Card"
import { HelloWorld } from "./components/HelloWorld";
import { HelloVar } from "./components/HelloVar"
import { HelloUser } from "./components/HelloUser"
import { HelloProps } from "./components/HelloProps"
import { ButtonProps } from './components/ButtonProps';
import { ButtonAlert } from './components/ButtonAlert';
import { Counter } from "./components/Counter";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HelloWorld></HelloWorld>
        <HelloVar />
        <HelloUser></HelloUser>
        <Card></Card>
        <HelloProps name="Abel Cabeza" age={45}></HelloProps>
        <ButtonProps text="Texto del botón"></ButtonProps>
        <ButtonAlert></ButtonAlert>
        <Counter></Counter>
      </header>
    </div>
  );
}

export default App;
