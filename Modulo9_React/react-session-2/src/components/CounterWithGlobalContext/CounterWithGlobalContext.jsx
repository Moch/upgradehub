import { React,  useContext, useState } from "react"

import { CounterContext } from "../../shared/contexts/CounterContext";


export function CounterWithGlobalContext(props) {

  const [count, setCount] = useState(useContext(CounterContext));

  return (
    <div>
      <p>The count is: {count}</p>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(count * 2)}>*2</button>
      <button onClick={() => setCount(count / 2)}>/2</button>
      <button onClick={() => setCount(0)}>reset</button>
    </div>
  );
}