import { React } from "react"


/* export function ListProps(props) {

  const list = [];

  for (let i = 0; i < props.list.length; i++) {
    list.push(<li key={i}>{props.list[i]}</li>)    
  }

  return (
    <ul>{list}</ul>
  );
} */


export function ListProps(props) {

  return (
    <ul>{props.list.map((item,i) => <li key={i}>{item}</li>)}</ul>
  );
}