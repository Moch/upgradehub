import React from "react";
import "./GalleryForm.scss"


export function GalleryForm(props) {

  const submitForm = (e) => {
    e.preventDefault(); 
    
    const title = document.querySelector("#title").value;
    const imgUrl = document.querySelector("#imgUrl").value;
    
    props.fnAddGalleryItem({title, imgUrl})

  }

  return (
    <form onSubmit={submitForm}>
      <label>
        <p>title</p>
        <input id="title" type="text"/>
      </label>
      <label>
        <p>img url</p>
        <input id="imgUrl" type="text"/>
      </label>
      <input className="gallery__formButton" type="submit" value="Enviar" />
    </form>
  );
}