import React from "react";


export function GalleryList(props) {

  return (
    <div>
      {props.list.map((e,i) => 
        <figure key={i}>
          <img alt="" src = {e.imgUrl}/>
          <figcaption>{e.title}</figcaption>
          <button onClick={() => props.rmItem(i)}>X</button>
        </figure>
      )}
    </div>
  );
}