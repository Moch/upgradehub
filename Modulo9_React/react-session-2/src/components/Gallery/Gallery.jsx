import React, { useState } from "react";
import { GalleryForm } from "./component/GalleryForm/GalleryForm";
import { GalleryList } from "./component/GalleryList/GalleryList";


export function Gallery(props) {

  const [galleryList, setGalleryList] = useState(props.galleryList);

  const removeItem = (index) => {
    const localList = [...galleryList];
    localList.splice(index,1);
    setGalleryList(localList);
  }

  const addGalleryItem = (newItem) => {
    setGalleryList([...galleryList, newItem])
  }

  return (
    <div>
      <GalleryForm fnAddGalleryItem={addGalleryItem}></GalleryForm>
      <GalleryList list={galleryList} rmItem={removeItem}/>
    </div>
  );
}