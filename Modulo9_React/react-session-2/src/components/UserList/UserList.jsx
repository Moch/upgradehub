import { React } from "react"


export function UserList(props) {

  return (
    <ul>
      {props.list.map((item,i) => 
        <li key={i}>
          My name is {item.name}, 
          I am {item.age} years old
          and I am a {item.role}
        </li>)
      }
    </ul>
  );
}