Crea un componente llamado UserList que reciba como props `list` que contenga una lista de usuarios con la siguiente estructura:

```js

{​​name: 'Abel Cabeza Román', age: 25, role: 'profesor'}​​

```

Cada uno de los <li> de la lista tendrá que imprimir un texto como el siguiente:

`My name is Abel Cabeza Román, I am 25 years old and I am profesor.`
