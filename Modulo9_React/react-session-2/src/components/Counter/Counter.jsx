import { React, useState } from "react"


export function Counter(props) {

  const [count, setCount] = useState(0);

  return (
    <div>
      <p>The count is: {count}</p>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(count * 2)}>*2</button>
      <button onClick={() => setCount(count / 2)}>/2</button>
      <button onClick={() => setCount(0)}>reset</button>
    </div>
  );
}