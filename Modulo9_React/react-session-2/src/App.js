import logo from './logo.svg';
import './App.css';
import { ListProps } from './components/ListProps/ListProps';
import { UserList } from './components/UserList/UserList';
import { Counter } from "./components/Counter/Counter"; 
import { CounterWithContext } from "./components/CounterWithContext/CounterWithContext";
import { Gallery } from './components/Gallery/Gallery';
import { OverAge } from './components/OverAge/OverAge';
import { CounterContext } from "./shared/contexts/CounterContext";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ListProps list={["Hola", "soy", "un", "perro"]}></ListProps>
        <UserList list={
          [ {name: "Abel", age: 25, role: "professor"}
          , {name: "Ramiro", age: 30, role: "student"}
          ]}
        ></UserList>
        <Counter></Counter>
        <CounterContext.Provider value={30}>
          <CounterWithContext></CounterWithContext>
        </CounterContext.Provider>
        <Gallery galleryList={
          [ {title: 'Paisaje esplendido', imgUrl: 'https://unhabitatmejor.leroymerlin.es/sites/default/files/styles/header_category/public/2018-10/4%20paisaje%20macedonia.jpg?itok=AELknmF8'}
          , {title: 'Gato alegre', imgUrl: 'https://ichef.bbci.co.uk/news/640/cpsprodpb/150EA/production/_107005268_gettyimages-611696954.jpg'}
          , {title: 'Fly me to the moon', imgUrl: 'https://imagenes.20minutos.es/files/og_thumbnail/uploads/imagenes/2020/10/26/luna-llena.jpeg'}
          , {title: 'Gato alegre', imgUrl: 'https://ichef.bbci.co.uk/news/640/cpsprodpb/150EA/production/_107005268_gettyimages-611696954.jpg'}
          ]} />
        <OverAge age={20}></OverAge>
      </header>
    </div>
  );
}

export default App;
