import React from 'react';
import { Button } from 'primereact/button';


export function HomePage(){
    return(
        <div>
            <span className="b-icon b-icon--big icon-japanese-shuriken"></span>
            <span className="b-icon icon-sunset-fuji-mountain"></span>
            <span className="b-icon b-icon--small b-icon--third icon-ninja-portrait"></span>
            <span className="b-icon b-icon--sec icon-japanese-flower"></span>

            <Button label="Save" />

            <span className="b-icon b-icon--sec icon-japanese-flower"></span>
            <span className="b-icon b-icon--small b-icon--third icon-ninja-portrait"></span>
            <span className="b-icon icon-sunset-fuji-mountain"></span>
            <span className="b-icon b-icon--big icon-japanese-shuriken"></span>

        </div>
    )
}