import React from 'react';
import './AnimesGallery.scss';

export function AnimesGallery(props) {

    return (
        <div className="c-animes-gallery">
            <div className="row">
                {props.animes.map((anime, index) =>
                    <div key={index} className={index < 3 ? "col-12 col-sm-12 col-md-4 col-lg-4" : "col-12 col-sm-6 col-md-3 col-lg-3"}>
                        <figure className="c-animes-gallery__figure">
                            <img src={anime.attributes.posterImage.medium} alt={anime.attributes.canonicalTitle} />
                            <figcaption className="c-animes-gallery__title"><span className="b-icon b-icon--quaternary icon-japanese-shuriken p-4"></span>{anime.attributes.canonicalTitle}<span className="b-icon b-icon--quaternary icon-japanese-shuriken p-4"></span></figcaption>
                        </figure>
                    </div>)}
            </div>
        </div>)
}