import React, { useEffect, useState } from 'react';
import { API } from '../../shared/consts/api.consts';
import { AnimesGallery } from './components/AnimesGallery/AnimesGallery';
import { Paginator } from 'primereact/paginator';
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
import Swiper from 'swiper';

SwiperCore.use([Navigation, Pagination]);

const itemsPerPage = 19;

export function AnimesPage() {

    const [animes, setAnimes] = useState([]);
    const [first, setFirst] = useState(0);
    const [totalRecords, setTotalRecords] = useState(0);


    const getAnimes = (offset = 0) => {
        API.get(`anime?page[limit]=${itemsPerPage}&page[offset]=${offset}`).then(res => {
            setAnimes(res.data.data);
            setTotalRecords(res.data.meta.count)
        })
    }

    const changePage = ($event) => {
        console.log($event)
        setFirst($event.first);
        getAnimes($event.first);
    }

    const createSlider = () => {
        const swiperContainer$$ = document.querySelector('.swiper-container');
        const swiper = new Swiper(swiperContainer$$, {
            // Optional parameters
            direction: 'horizontal',

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });
    }

    useEffect(getAnimes, []);

    useEffect(createSlider, [animes]);

    return (<div>
        <div className="g-swiper swiper-container my-5" style={{ height: '300px' }}>
            <div className="swiper-wrapper">
                {animes.slice(0, 3).map((anime, index) => <div className="swiper-slide"><img src={anime.attributes.coverImage.original} alt="" /></div>)}
            </div>
            <div className="swiper-pagination"></div>

            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
        </div>

        <AnimesGallery animes={animes} />
        <Paginator className="g-primereact-paginator" first={first} rows={itemsPerPage}
            totalRecords={totalRecords} onPageChange={changePage} />
        <Paginator className="g-primereact-paginator g-primereact-paginator--blue-fantasy" first={first} rows={itemsPerPage}
            totalRecords={totalRecords} onPageChange={changePage} />

    </div>)
}