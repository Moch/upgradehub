import React from 'react';
import {Route, Switch} from 'react-router-dom';
import { AnimesPage } from '../../../pages/AnimesPage/AnimesPage';
import { HomePage } from '../../../pages/HomePage/HomePage';

export function Routes() {

    return (<Switch>
         <Route path="/animes">
            <AnimesPage/>
        </Route>
        <Route path="/">
            <HomePage/>
        </Route>
    </Switch>)
}