import React from 'react';
import {Link} from 'react-router-dom';

export function Menu() {

    return (
        <nav>
            <Link to="/">Home</Link>
            <Link to="/animes">Animes</Link>
        </nav>
    )
}