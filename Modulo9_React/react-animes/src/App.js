import logo from './logo.svg';
import './App.css';
import { HomePage } from './pages/HomePage/HomePage';
import { Routes } from './core/components/Routes/Routes';
import { BrowserRouter as Router } from 'react-router-dom';
import { Menu } from './core/components/Menu/Menu';

function App() {
  return (
    <Router>
      <div className="App">
        <div className="App-header">
          <div className="container">
            <Menu/>
            <Routes />
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
