

const math = require("./math");

const a = Number.parseFloat(process.argv[3]);
const b = Number.parseFloat(process.argv[4]);

let f;
switch (process.argv[2]) {
  case "--suma":
  case "+":
    f = math.suma;
    break;
  case "--resta":
  case "-":
    f = math.resta;
    break;
  case "--multi":
  case "*":
  f = math.multi;
  break;
  case "--div":
  case "/":
    f = math.div;
    break;
}

f(a,b);