

function suma(a, b) {
  console.log(a + b);
}

function resta(a, b) {
  console.log(a - b);
}

function multi(a, b) {
  console.log(a * b);
}

function div(a, b) {
  console.log(a / b);
}


module.exports = 
  { suma
  , resta
  , multi
  , div
  }