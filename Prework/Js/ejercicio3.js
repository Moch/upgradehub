var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 
              'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
                            
function dniValido(dni,letra) {
    if (dni < 0 || dni > 99999999 || dni%1 != 0) {
        console.log("El número proporcionado no es válido");
    } else if (letra === letras[dni%23] ) {
        console.log("DNI Válido");
    } else {
        console.log("DNI Incorrecto");
    }
}

dniValido(12.67,'A');
dniValido(0,"T");
dniValido(53732486,"R");
dniValido(2354364,"Y");

// var userDNI = window.prompt("Introduce tu DNI");
// var userLet = window.prompt("Introduce la letra de tu DNI");
// dniValido(userDNI,userLet);