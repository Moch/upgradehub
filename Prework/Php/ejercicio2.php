<?php

function contieneTodasLasVocales(string $s):bool{
    // NO FUNCIONA PARA LETRAS CON TILDE
    $foundA = False;
    $foundE = False;
    $foundI = False;
    $foundO = False;
    $foundU = False;
    for($i = 0; $i < strlen($s); $i++){
        if ($s[$i] == 'a' || $s[$i] == 'A' || $s[$i] == 'á' || $s[$i] == 'Á'){
            $foundA = True;
        }
        if ($s[$i] == 'e' || $s[$i] == 'E' || $s[$i] == 'é' || $s[$i] == 'É'){
            $foundE = True;
        }
        if ($s[$i] == 'i' || $s[$i] == 'I' || $s[$i] == 'í' || $s[$i] == 'Í'){
            $foundI = True;
        }
        if ($s[$i] == 'o' || $s[$i] == 'O' || $s[$i] == 'ó' || $s[$i] == 'Ó'){
            $foundO = True;
        }
        if ($s[$i] == 'u' || $s[$i] == 'U' || $s[$i] == 'ú' || $s[$i] == 'Ú' || $s[$i] == 'ü' || $s[$i] == 'Ü'){
            $foundU = True;
        }
        // var_dump($foundA);
        // var_dump($foundE);
        // var_dump($foundI);
        // var_dump($foundO);
        // var_dump($foundU);
        // echo $s[$i];
        // echo "-------------- \n";
    }
    $res = $foundA && $foundE && $foundI && $foundO && $foundU;
    return($res);
}


function printVocales($s){
    if (contieneTodasLasVocales($s)){
        echo "LA PALABRA CONTIENE LAS 5 VOCALES";
    } else {
        echo "NO CONTIENE TODAS LAS VOCALES";
    }
}


$msg = "aéióu";
$msg = $_GET['msg'];
printVocales($msg);
