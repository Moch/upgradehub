<?php

function numToWeekDay($n):string{
    $res = "No has introducido un número entero entre el 1 y el 7";
    switch($n){
        case 1.0:
            $res = "Lunes"; break;
        case 2.0:
            $res = "Martes"; break;
        case 3.0:
            $res = "Miércoles"; break;
        case 4.0:
            $res = "Jueves"; break;
        case 5.0:
            $res = "Viernes"; break;
        case 6.0:
            $res = "Sábado"; break;
        case 7.0:
            $res = "Domingo"; break;
    }
    return($res);
}


echo numToWeekDay(-5) . "\n";
echo numToWeekDay(0) . "\n";
echo numToWeekDay(1.7) . "\n";
echo numToWeekDay(1) . "\n";
echo numToWeekDay(2) . "\n";
echo numToWeekDay(3) . "\n";
echo numToWeekDay(4) . "\n";
echo numToWeekDay(5) . "\n";
echo numToWeekDay(6) . "\n";
echo numToWeekDay(7) . "\n";
echo numToWeekDay(8) . "\n";