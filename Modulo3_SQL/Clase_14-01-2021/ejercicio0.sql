CREATE DATABASE ej0;

USE ej0;

CREATE TABLE Cliente (
  codigoCliente int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50) NOT NULL,
  apellidos varchar(50),
  empresa varchar(50),
  puesto varchar(50),
  direccion varchar(50),
  poblacion varchar(25) DEFAULT "Écija",
  cp varchar(5) DEFAULT "41400",
  provincia varchar(25) DEFAULT "Sevilla",
  telefono varchar(9),
  fechaNacimiento date
);

CREATE TABLE Articulo (
  codigoArticulo int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50) NOT NULL,
  descripcion varchar(50) NOT NULL,
  precioUnitario float(6, 2) NOT NULL UNSIGNED,
  moneda char(1) DEFAULT "€",
  enStock int,
  stockSeguridad int,
  imagen varchar(500)
);

CREATE TABLE Compra (
  codigoCompra int AUTO_INCREMENT PRIMARY KEY,
  codigoCliente int,
  codigoArticulo int,
  fecha date,
  unidades int,
  FOREIGN KEY codigoCliente REFERENCES Cliente(codigoCliente),
  FOREIGN KEY codigoArticulo REFERENCES Articulo(codigoArticulo)
);

INSERT INTO
  Cliente (
    nombre,
    apellidos,
    empresa,
    puesto,
    direccion,
    telefono,
    fechaNacimiento
  )
VALUES
  (
    "José",
    "Fernández Ruiz",
    "Estudio Cero",
    "Gerente",
    "Cervantes, 13",
    "656789043",
    "1968-06-13"
  ),
  (
    "Luis",
    "Fernández Chacón",
    "Beep",
    "Dependiente",
    "Aurora, 4",
    "675894566",
    "1982-05-24"
  ),
  (
    "Antonio",
    "Ruiz Gómez",
    "Comar",
    "Dependiente",
    "Osuna, 23",
    "654345544",
    "1989-08-06"
  ),
  (
    "Andrea",
    "Romero Vázquez",
    "Estudio Cero",
    "Dependiente",
    "Cervantes, 25",
    "646765657",
    "1974-11-23"
  ),
  (
    "José",
    "Pérez Pérez",
    "Beep",
    "Gerente",
    "Córdoba, 10",
    "645345543",
    "1978-04-10"
  );

INSERT INTO
  Articulo (
    nombre,
    descripcion,
    precioUnitario,
    enStock,
    stockSeguridad
  )
VALUES
  (
    "NETGEAR switch prosafe",
    "Switch 8 puertos GigabitEthernet",
    125.00,
    3,
    2
  ),
  (
    "Switch SRW224G4-EU de Linksys",
    "CISCO switch 24 puertos 10/100",
    202.43,
    2,
    2
  ),
  (
    "Switch D-link",
    "D-Link smart switch 16 puertos",
    149.90,
    7,
    4
  ),
  (
    "Switch D-link",
    "D-Link smart switch 46 puertos",
    489.00,
    4,
    2
  );

INSERT INTO
  Compra (codigoCliente, codigoArticulo, fecha, unidades)
VALUES
  (1, 1, "2010-10-13", 2),
  (1, 2, "2010-10-13", 1),
  (2, 3, "2010-10-15", 1),
  (2, 4, "2010-10-15", 1),
  (3, 1, "2010-10-15", 2),
  (4, 2, "2010-10-15", 1),
  (5, 3, "2010-10-15", 3),
  (1, 4, "2010-10-16", 1),
  (1, 1, "2010-10-16", 2),
  (2, 2, "2010-10-17", 1),
  (3, 3, "2010-10-18", 4),
  (4, 4, "2010-10-19", 2),
  (5, 1, "2010-10-19", 1);

SELECT
  nombre,
  apellidos,
  telefono
FROM
  cliente
WHERE
  nombre LIKE "%Jose%"
  OR nombre LIKE "%Luis%"
ORDER BY
  nombre;

SELECT
  nombre,
  fecha_nacimiento
FROM
  cliente
ORDER BY
  fecha_nacimiento;

SELECT
  nombre,
  apellidos
FROM
  cliente
WHERE
  telefono IS NULL;

SELECT
  nombre,
  unidades_stock
FROM
  articulo
WHERE
  unidades_stock < 4;

SELECT
  nombre,
  descripcion,
  imagen,
  precio_unidad
FROM
  articulo
WHERE
  precio_unidad < 200;

SELECT
  count(*) AS "num productos"
FROM
  articulo
WHERE
  nombre LIKE "%D-link%";

SELECT
  nombre,
  descripcion
FROM
  articulo
WHERE
  nombre LIKE '%D-link%';

SELECT
  count(*)
FROM
  compra;

SELECT
  count(*)
FROM
  compra
WHERE
  fecha BETWEEN '2010-01-01'
  AND '2010-12-31';

SELECT
  count(*)
FROM
  compra
WHERE
  cod_cli = 4;

SELECT
  nombre
FROM
  cliente
WHERE
  codigoCliente = 4;

SELECT
  sum(unidad)
FROM
  compra
WHERE
  cod_art = 2;

SELECT
  nombre
FROM
  articulo
WHERE
  codigoArticulo = 2;

SELECT
  max(precioUnidad)
FROM
  articulo;

SELECT
  nombre
FROM
  articulo
WHERE
  precioUnidad = 489.00;

SELECT
  avg(precioUnidad)
FROM
  articulo;

SELECT
  nombre,
  precioUnidad
FROM
  articulo
WHERE
  precioUnidad > 241.58;

SELECT
  count(*)
FROM
  articulo
WHERE
  precioUnidad > 241.58;

SELECT
  DISTINCT nombre
FROM
  cliente;

SELECT
  nombre,
  telefono,
  (year(NOW()) - year(fechaNacimiento)) AS edad
FROM
  cliente
WHERE
  (year(NOW()) - year(fechaNacimiento)) BETWEEN 30
  AND 40;