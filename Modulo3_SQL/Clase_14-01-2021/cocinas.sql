create database cocinas;

use cocinas;

create table ModeloCocina 
  ( id     int(10)      primary key auto_increment
  , nombre varchar(100)
  );

create table Montador 
  ( nif       varchar(20)  primary key
  , direccion varchar(100)
  , telefono  varchar(50)
  , nombre    varchar(100)
  );

create table Cliente 
  ( nif       varchar(20)  primary key
  , direccion varchar(100)
  , telefono  varchar(50)
  , nombre    varchar(100)
  );

create table Monta 
  ( idMontaje     int(10)     primary key auto_increment
  , idCocina      int(10)
  , nifMontador   varchar(20)
  , numeroCocinas int(1000)   unsigned
  , foreign key (idCocina)    references ModeloCocina(id)
  , foreign key (nifMontador) references Montador(nif)
  );

create table Compra
  ( idCompra    int(10) primary key auto_increment
  , idCocina    int(10)
  , nifCliente  varchar(20)
  , fechaCompra date
  , foreign key (idCocina) references   ModeloCocina(id)
  , foreign key (nifCliente) references Cliente(nif)
  );