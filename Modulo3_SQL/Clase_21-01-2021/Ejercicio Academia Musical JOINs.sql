/ / EJERCICIO ACADEMIA DE MUSICA JOINS CREATE TABLE socios (
  documento char(8) NOT NULL,
  nombre varchar(30),
  domicilio varchar(30),
  PRIMARY KEY(documento)
);

CREATE TABLE inscritos (
  documento char(8) NOT NULL,
  asignatura varchar(15) NOT NULL,
  año year,
  matriculaPag enum ('s', 'n'),
  FOREIGN KEY (documento) REFERENCES socios (documento),
  PRIMARY KEY(documento, asignatura, año)
);

INSERT INTO
  socios
VALUES
  ('111111', 'Juan Perez', 'Colon 234');

INSERT INTO
  socios
VALUES
  ('222222', 'Maria Lopez', 'Sarmiento 465');

INSERT INTO
  socios
VALUES
  ('333333', 'Antonio Juarez', 'Caseros 980');

INSERT INTO
  socios
VALUES
  ("999999", "Pedro García", "Avenida Logroño SN");

INSERT INTO
  inscritos
VALUES
  ('111111', 'solfeo', '2017', 's');

INSERT INTO
  inscritos
VALUES
  ('111111', 'solfeo', '2018', 'n');

INSERT INTO
  inscritos
VALUES
  ('222222', 'solfeo', '2017', 's');

INSERT INTO
  inscritos
VALUES
  ('222222', 'guitarra', '2018', 's');

INSERT INTO
  inscritos
VALUES
  ('222222', 'solfeo', '2018', 's');

INSERT INTO
  inscritos
VALUES
  ('333333', 'guitarra', '2018', 'n');

INSERT INTO
  inscritos
VALUES
  ('333333', 'trompeta', '2018', 'n');

SELECT
  socios.nombre,
  inscritos.*
FROM
  socios
  JOIN inscritos ON socios.documento = inscritos.documento;

SELECT
  socios.nombre,
  inscritos.asignatura,
  inscritos.año
FROM
  socios
  JOIN inscritos ON socios.documento = inscritos.documento
WHERE
  inscritos.año = 2018;

SELECT
  socios.nombre,
  inscritos.*
FROM
  socios
  JOIN inscritos ON socios.documento = inscritos.documento
WHERE
  inscritos.documento = "222222";