create database logistica;

-- show databases;
use logistica;

create table Camion 
  ( matricula char(10)  primary key
  , modelo    char(255)
  , tipo      char(255)
  , potencia  int
  );

create table Camionero
  ( dni       char(8)       primary key
  , nombre    char(255)     not null
  , telefono  char(255)
  , direccion char(255)
  , poblacion varchar(1000)
  , salario   int
  );

create table Conduce
  ( camionero char(8)
  , camion    char(10)
  , fecha     date
  , foreign key (camionero) references Camionero(dni)
  , foreign key (camion)    references Camion(matricula)
  );

create table Provincia 
  ( cp     char(5)   primary key
  , nombre char(100) not null
  );

create table Paquete 
  ( codigoPqt    int           primary key
  , descripcion  varchar(2000)
  , destinatario char(255)
  , direccion    varchar(1000) not null
  , foreign key (camionero) references Camionero(dni)
  , foreign key (provincia) references Provincia(cp)
  );

