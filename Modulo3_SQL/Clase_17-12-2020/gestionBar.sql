create database gestionBar;
-- show databases;
use gestionBar;
create table Cliente (
  idCliente int(4) primary key,
  nombre varchar(30) not null,
  telefono char(9),
  fechaAlta date,
  alergias varchar(500)
);
/* show tables; */
-- describe cliente;
create table Pedido (
  idPedido int(8),
  nomResponsable char(20),
  descuento int(2),
  fecha date,
  codCliente int(4),
  primary key (idPedido),
  foreign key (codCliente) references Cliente(idCliente)
);
create table Producto (
  idProducto int primary key,
  nombreP varchar(1000) not null,
  marca char(255),
  tipo char(255),
  precio float,
  cantidad int
);
create table Contiene (
  pedido int(8),
  producto int,
  foreign key (pedido) references Pedido(idPedido),
  foreign key (producto) references Producto(idProducto)
);
create table Proveedor (
  cif char(8) primary key,
  nombreProv char(255) not null,
  direccionF varchar(1000),
  nombContacto char(255),
  telefono char(255),
  pago varchar(1000)
);
create table Trae (
  proveedor char(8),
  producto int,
  foreign key (proveedor) references Proveedor(cif),
  foreign key (producto) references Producto(idProducto)
);