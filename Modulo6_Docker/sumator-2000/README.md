# Instrucciones

Este ejercicio es una sencilla calculadora que suma los 2 números que se le indican.

Hemos llegado un poco 'perjudicados' de una fiesta, y nos hemos puesto a hacer la tarea que nos ha pedido
nuestro jefe. No sabemos qué puede estar ocurriendo pero no conseguimos implementar la funcionalidad de sumar 2 números.

**¡Por favor ayúdanos!**

Encuentra el error que hace que no se sumen correctamente los 2 números, corrígelo y háznos un Merge Request!

## Levantar un servidor web
Tenéis adjunto un fichero *docker-compose.yaml* con el que utilizando Docker Compose podéis levantar un servidor web cuyo 
document root está apuntando directamente al directorio raiz del proyecto.

Para ello solamente tenéis que ejecutar:

````bash
docker-compose up -d
````

Y después acceder a vuestro navegador con la siguiente url:

    http://localhost:8888/

**NOTA:** Es posible que igual localhost no os funcione y tengáis que usar 192.168.99.100 o la IP 
que os haya asignado docker-machine 

**NOTA 2:** En el caso que tengais ocupado el puerto 8888 cambiad el puerto a alguno libre.
