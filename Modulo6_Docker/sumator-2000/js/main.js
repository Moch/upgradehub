document.getElementById('suma').addEventListener('click', () => {
    //obtenemos el valor del sumando 1
    let sum1 = parseInt(document.getElementById('sum1').value);

    //obtenemos el valor del sumando 2
    let sum2 = parseInt(document.getElementById('sum2').value);

    //sumamos los 2 números
    let resultado = sum1 + sum2;

    //añadimos el resultado al html de la alerta
    document.getElementById('result').innerHTML = resultado;

    //mostrar alerta con el resultado
    document.querySelector('.alert').classList.remove('collapse');
    document.querySelector('.alert').classList.add('show');
});
