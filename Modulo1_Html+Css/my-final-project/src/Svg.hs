{-# LANGUAGE     OverloadedStrings       #-}
{-# OPTIONS_GHC -fno-warn-type-defaults  #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}


module Svg where

import Text.Blaze.Svg11 ((!))
import Text.Blaze.Svg11 as S
import Text.Blaze.Svg11.Attributes as A
import Text.Blaze.Svg.Renderer.String



main :: IO ()
main = compileSvg

compileSvg :: IO ()
compileSvg =
    mapM_ f allSvg
  where
    f (fileName , svgCode) =
      writeFile ("../assets/img/" ++ fileName) (renderSvg $ docTypeSvg svgCode)


allSvg :: [ (FilePath , Svg) ]
allSvg =
  [ (,) "mosaic.svg" geometricalMosaic
  ]



geometricalMosaic :: Svg
geometricalMosaic =
  svg
    ! A.viewbox "0 0 1 1"
    ! A.preserveaspectratio "xMinYMin meet"
    ! A.fill   "rgb(235,235,235)"
    ! A.stroke "rgb(235,235,235)"
    $ do
      corner
      corner ! A.transform (rotateAround 90  0.5 0.5)
      corner ! A.transform (rotateAround 180 0.5 0.5)
      corner ! A.transform (rotateAround 270 0.5 0.5)
      semicircunference
      semicircunference ! A.transform (rotateAround 180 0.5 0.5)
      semihexagon
      semihexagon ! A.transform (rotateAround 180 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 0  ) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 72 ) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 144) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 216) 0.5 0.5)
      pentagonSide ! A.transform (rotateAround (180 + 288) 0.5 0.5)
  where
    s = 0.02 :: Float
    k = 0.12 :: Float
    --------------------------------------------------
    corner =
      S.path
        ! A.strokeOpacity "0"
        ! A.d cornerDirs
    cornerDirs =
      mkPath $ do
        m   (0.1 - s)  0
        l   (0.1 + s)  0
        l   0          (0.1 + s)
        l   0          (0.1 - s)
        S.z
    --------------------------------------------------
    semicircunference =
      S.path
        ! A.fillOpacity "0"
        ! (A.strokeWidth $ S.toValue $ 1.5*s)
        ! A.d semicircunferenceDirs
    semicircunferenceDirs =
      mkPath $ do
        m   0  (0.5 - k)
        aa  k  k  0  True True 0  (0.5 + k)
    --------------------------------------------------
    r1 = k + s
    r2 = k - s
    cos60 = 0.5
    sin60 = 0.5 * sqrt 3
    semihexagon =
      S.path
        ! A.strokeOpacity "0"
        ! A.d semihexagonDirs
    semihexagonDirs =
      mkPath $ do
        m   (0.5 - r1)          0
        l   (0.5 - r1 * cos60)  (r1 * sin60)
        l   (0.5 + r1 * cos60)  (r1 * sin60)
        l   (0.5 + r1)          0
        l   (0.5 + r2)          0
        l   (0.5 + r2 * cos60)  (r2 * sin60)
        l   (0.5 - r2 * cos60)  (r2 * sin60)
        l   (0.5 - r2)          0
        S.z
    --------------------------------------------------
    α = (180 - 90 - 36) * pi / 180
    -- α = 180 - 90 - 36
    pentagonSide =
      S.path
        ! A.strokeOpacity "0"
        ! A.d pentagonSideDirs
    pentagonSideDirs =
      mkPath $ do
        m   (0.5 - r1 * cos α)  (0.5 + r1 * sin α)
        l   (0.5 + r1 * cos α)  (0.5 + r1 * sin α)
        l   (0.5 + r2 * cos α)  (0.5 + r2 * sin α)
        l   (0.5 - r2 * cos α)  (0.5 + r2 * sin α)
        S.z

