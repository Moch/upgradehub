
const workWithUsBtn = document.querySelector(".js-workWithUsBtn");

function rotate(){
  workWithUsBtn.classList.add("rotateX");
  setTimeout(() => workWithUsBtn.classList.remove("rotateX"), 3000);
}

function addRotation(){
  rotate();
  workWithUsBtn.removeEventListener("mousemove", addRotation);
  setTimeout(() => workWithUsBtn.addEventListener("mousemove", addRotation), 5000);
}

workWithUsBtn.addEventListener("mousemove", addRotation);