/* Galeria de fotos */

var galeria = [
  { foto: "galeria1.jpg", texto: "Imagen 1" } /* posicion 0 */,
  { foto: "galeria2.jpg", texto: "Imagen 2" } /* posicion 1 */,
  { foto: "galeria3.jpg", texto: "Imagen 3" } /* posicion 2 */,
  { foto: "galeria4.jpg", texto: "Imagen 4" } /* posicion 3 */,
  { foto: "galeria1.jpg", texto: "Imagen 5" } /* posicion 4 */,
  { foto: "galeria2.jpg", texto: "Imagen 6" } /* posicion 5 */,
  { foto: "galeria3.jpg", texto: "Imagen 7" } /* posicion 6 */,
  { foto: "galeria4.jpg", texto: "Imagen 8" } /* posicion 7 */,
  { foto: "galeria1.jpg", texto: "Imagen 9" } /* posicion 8 */,
  { foto: "galeria2.jpg", texto: "Imagen 10" } /* posicion 9 */,
  { foto: "galeria3.jpg", texto: "Imagen 11" } /* posicion 10 */,
  { foto: "galeria4.jpg", texto: "Imagen 12" } /* posicion 11 */,
];

var indices = [0, 1, 2];
var intervalo = null;

function incrementarIndices() {
  for (var i = 0; i < indices.length; i++) {
    indices[i]++;
    if (indices[i] == galeria.length) {
      indices[i] = 0;
    }
  }
}

function decrementarIndices() {
  for (var i = 0; i < indices.length; i++) {
    indices[i]--;
    if (indices[i] < 0) {
      indices[i] = galeria.length - 1;
    }
  }
}

function mostrarImagenes() {
  for (var i = 0; i < indices.length; i++) {
    var foto = document.getElementById("foto" + i);
    if (foto) {
      foto.src = "../assets/imagenes/" + galeria[indices[i]].foto;
    }
    var texto = document.getElementById("texto" + i);
    if (texto) {
      texto.innerText = galeria[indices[i]].texto;
    }
  }
}

function anterior() {
  decrementarIndices();
  mostrarImagenes();
}

function proxima() {
  incrementarIndices();
  mostrarImagenes();
}

function desactivarMovimiento() {
  if (intervalo) {
    clearInterval(intervalo);
    intervalo = null;
  }
}

function activarMovimiento() {
  if (intervalo == null) {
    intervalo = setInterval(proxima, 3000);
  }
}

function ActivarDesactivar() {
  var check = document.getElementById("mover");
  if (check) {
    if (check.checked) {
      activarMovimiento();
    } else {
      desactivarMovimiento();
    }
  }
}

mostrarImagenes();
activarMovimiento();

/*
var indice=0;

function mostrarFoto(){
  var foto = document.getElementById('galeria-foto');
  if (foto){
      foto.src = "../assets/imagenes/"+galeria[indice].foto;
  }
  var texto = document.getElementById('galeria-texto');
  if (texto){
      texto.innerText = galeria[indice].texto;
  }
}

function anterior(){
    indice--;
    if (indice < 0) indice=galeria.length - 1;
    mostrarFoto();
}

function proxima(){
    indice++;
    if (indice == galeria.length) indice = 0;
    mostrarFoto();
}
*/
/*
function anterior(){    
    if (indice > 0){
        indice--;
        mostrarFoto();
    }
}

function proxima(){
    if (indice <  galeria.length - 1){
        indice++;  
        mostrarFoto();
    }     
}


mostrarFoto();

*/
