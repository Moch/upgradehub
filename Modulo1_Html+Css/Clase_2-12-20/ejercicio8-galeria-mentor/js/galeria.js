/* Galeria de fotos */

var galeria = [
   {foto:'galeria1.jpg', texto:'Imagen 1'}, /* posicion 0 */
   {foto:'galeria2.jpg', texto:'Imagen 2'}, /* posicion 1 */
   {foto:'galeria3.jpg', texto:'Imagen 3'}, /* posicion 2 */
   {foto:'galeria4.jpg', texto:'Imagen 4'}, /* posicion 3 */
];

var indice=0;

function mostrarFoto(){
  var foto = document.getElementById('galeria-foto');
  if (foto){
      foto.src = "./assets/imagenes/"+galeria[indice].foto;
  }
  var texto = document.getElementById('galeria-texto');
  if (texto){
      texto.innerText = galeria[indice].texto;
  }
}

function anterior(){
    indice--;
    if (indice < 0) indice=galeria.length - 1;
    mostrarFoto();
}

function proxima(){
    indice++;
    if (indice == galeria.length) indice = 0;
    mostrarFoto();
}

/*
function anterior(){    
    if (indice > 0){
        indice--;
        mostrarFoto();
    }
}

function proxima(){
    if (indice <  galeria.length - 1){
        indice++;  
        mostrarFoto();
    }     
}
*/

mostrarFoto();