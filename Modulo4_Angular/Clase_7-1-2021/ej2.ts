
const cat = {​​name: 'Gris', age: 2, race: 'Azul ruso'}​​

const cat2 = {​​name: 'Yuna', age: 5, race: 'Siamés', color: 'white'}​​

interface Cat {
  name: string;
  age: number;
  race: string;
  color?: string;
}