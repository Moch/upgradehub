<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Service\EmojiTranslator;

class DefaultController extends AbstractController
{
  /**
   * @Route("/saludar/{nombre}")
   */
  public function saludar($nombre, EmojiTranslator $emojiT)
  {
    dump($nombre);
    return $this->render('saludo.html.twig', ['name' => $nombre]);
  }

  /**
   * @Route("/formulario")
   */
  public function formulario()
  {
  }

  /**
   * @Route("/procesarFormulario")
   */
  public function procesarFormulario(Request $r)
  {
    $nombre = $r->request->get("nombre");
    $apellidos = $r->request->get("apellidos");

    dd($nombre, $apellidos);
  }
}
