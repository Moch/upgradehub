<?php

require_once "./Poligono.php";


class Circulo extends Poligono
{

  public $r;

  public function __construct($r)
  {
    $this->r = $r;
  }

  public function area()
  {
    return pi() * $this->r * $this->r;
  }
}
