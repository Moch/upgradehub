<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body style="display: flex; justify-content: center; align-items: center; height: 100vh; font-size: 100px;">
  <?php


  function piramide($n)
  {

    for ($i = 1; $i <= $n; $i++) {
      echo str_repeat("&nbsp", $n - $i) . str_repeat("*", $i) . "<br>";
    }
  }


  piramide(5);
  ?>
</body>

</html>