# Ejercicio Excepciones

El objetivo de esta práctica es intentar controlar un error en el caso de que una petición HTTP a un API
sea incorrecta y produzca un error.

Es mucho mejor controlar las excepciones nosotros y actuar en consecuencia, que dejar que la excepción finalice
nuestro programa.

Una vez controlado el error, cread un script que utilizando la misma API https://swapi.dev, nos diga los nombres
de los planetas que salen en la película "La amenaza fantasma". Ojo que los nombres están en inglés.

**Tip:** Mirad la documentación de Guzzle para ver qué excepciones lanza: http://docs.guzzlephp.org/en/stable/quickstart.html#exceptions
 
**Bonus:** Cuando tengáis el código resuelto, hacer refactoring para que el código sea orientado a objetos.

La idea es que haya una clase encargada de realizar las peticiones HTTP al api y vosotros solamente le pregunteis
mediante una llamada a un método que os devuelva una lista de objetos planeta.

## Instalación

Si ya tenéis la imagen php-environment construida de prácticas anteriores, solamente tendréis que levantar el contenedor
ejecutando lo siguiente:

```
docker-compose up -d
```

Si no tenéis la imagen construida, modificar el fichero docker-compose.yml eliminando la entrada **image** y 
descomentando las lineas de la entrada **build**, y reemplazando las entradas user, uid y environment para que
tengan los valores adecuados de vuestro usuario local.

Entramos en el contenedor usando la opción **-u**:

 ```
 docker-compose exec -u kiko webserver bash
 ```
 
 e instalamos las dependencias:

```
composer install
```

Y ya tendríais acceso a la aplicación en la url: http://localhost:8888/demo.php