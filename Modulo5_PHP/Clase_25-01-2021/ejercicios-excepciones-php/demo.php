<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

require 'vendor/autoload.php';

$apiUrl = "https://swapi.dev/api/";

$client = new Client();

//hacemos la petición a un endpoint invalido
try {
  $res = $client->request('GET', $apiUrl . 'people/1');

  echo "El nombre del personaje con ID 1 es: " . json_decode($res->getBody(), true)['name'] . "\n";
} catch (ClientException $e) {
  echo "Error en la API";
}
