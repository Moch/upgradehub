<?php

require 'vendor/autoload.php';

use RicardoFiorani\Legofy\Legofy;

// The multiplier for the amount of legos on your image, or "legolution" :)
$resolutionMultiplier = 3;

// When set to true it will only use lego colors that exists in real world.
$useLegoPalette = false;

$legofy = new Legofy();

// $source can be any acceptable parameter for intervention/image
// Please see http://image.intervention.io/api/make
$source = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png';


$output = $legofy->convertToLego($source, $resolutionMultiplier, $useLegoPalette);

// Please see http://image.intervention.io/use/basics and http://image.intervention.io/use/http
echo $output->response();
