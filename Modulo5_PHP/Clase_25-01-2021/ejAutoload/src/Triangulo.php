<?php

namespace Geo;

use Geo\Base\Poligono;


class Triangulo extends Poligono
{

  public $b;
  public $h;

  function __construct($b, $h)
  {
    $this->b = $b;
    $this->h = $h;
  }

  public function area()
  {
    return $this->b * $this->h / 2;
  }
}
