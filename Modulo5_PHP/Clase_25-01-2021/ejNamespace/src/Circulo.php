<?php

namespace Geo;

require_once "Base/Poligono.php";

use Geo\Base\Poligono;


class Circulo extends Poligono
{

  public $r;

  public function __construct($r)
  {
    $this->r = $r;
  }

  public function area()
  {
    return pi() * $this->r * $this->r;
  }
}
