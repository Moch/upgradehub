<?php

namespace Geo;

require_once "Base/Poligono.php";

use Geo\Base\Poligono;


class Cuadrado extends Poligono
{

  public $l;

  function __construct($l)
  {
    $this->l = $l;
  }

  public function area()
  {
    return $this->l * $this->l;
  }
}
