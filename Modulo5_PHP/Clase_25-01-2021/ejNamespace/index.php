<?php


require_once "src/Circulo.php";
require_once "src/Cuadrado.php";
require_once "src/Triangulo.php";

use Geo\Circulo;
use Geo\Cuadrado;
use Geo\Triangulo;

$c1 = new Circulo(1);
$c2 = new Circulo(2);

$s1 = new Cuadrado(1);
$s2 = new Cuadrado(2);

$t1 = new Triangulo(1, 1);
$t2 = new Triangulo(1, 2);


echo "Áreas círculos: {$c1->area()} y {$c2->area()} <br>";
echo "Áreas cuadrados: {$s1->area()} y {$s2->area()} <br>";
echo "Áreas triángulos: {$t1->area()} y {$t2->area()} <hr>";
