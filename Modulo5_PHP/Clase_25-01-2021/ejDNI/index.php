<?php

require_once "vendor/autoload.php";

use NifValidator\NifValidator;

$dni = "56565656Y";

$valido = (NifValidator::isValid($dni)) ? "válido" : "inválido";

echo "El dni $dni es $valido";
