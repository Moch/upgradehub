<?php

function leerArchivo($path)
{

  $handle = fopen($path, "r");
  $line   = fgets($handle, 4096);
  $i = 1;
  while ($line !== false) {
    $n = str_word_count($line);
    echo "Línea $i, $n palabras: " . $line;
    echo "<br>";
    $i += 1;
    $line = fgets($handle, 4096);
  }
  fclose($handle);
}

leerArchivo("./lorem-ipsum.txt");
