Enunciado:

Dado el siguiente array:
$colores = ['blanco', 'verde', 'rojo'];

Escribir un script que muestre los colores de la siguientes maneras:

- Separados por comas

ej: blanco, verde, rojo

- Como una lista html, estando los elementos ordenados alfabéticamente

ej:

<ul>
    <li>blanco</li>
    <li>rojo</li>
    <li>verde</li>
</ul>
