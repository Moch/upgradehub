Crear una función que ‘milenialice’ un texto que se le pase, es decir:
_ los ‘que’ los convierte el ‘k’
porque => xq
_ Pone todo en mayúsculas indistintamente
_ Gu/Bu => w
_ Igual => = \* Sin apertura de ¡ ni ¿, ni acentuación
