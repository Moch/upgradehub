<?php

function normalizarNombre($s)
{

  if (strtolower(substr($s, -4)) == ".exe") {
    return strtoupper($s);
  }
  if (strtolower(substr($s, -3)) == ".db") {
    return strtolower($s);
  }
}


echo normalizarNombre("Pepe.eXe");
echo "<br>";
echo normalizarNombre("Ana.db");
