<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  if (count($_FILES) == 0 || $_FILES["archivo"]["error"] != 0) {
    echo
    "<form action='formEj1.php' method='POST' enctype='multipart/form-data'>
         <label for='archivo'>Selecciona un archivo de texto: </label>
         <br>
         <input type='file' name='archivo'>
         <hr>
         <label for='palabra'>Palabra para buscar: </label>
         <br>
         <input type='text' name='palabra'>
         <br> 
         <input type='submit' value='Enviar'>
      </form>";
  } else {
    copy($_FILES["archivo"]["tmp_name"], "quijote.txt");
    $word = strtolower($_POST["palabra"]);
    echo
    "La palabra " .
      $word .
      " aparece exactamente " .
      substr_count(strtolower(file_get_contents("quijote.txt")), $word) .
      " veces.";
  }
  ?>
</body>

</html>