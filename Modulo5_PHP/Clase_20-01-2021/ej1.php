<?php

function potenciaMenorQue10000($n)
{

  $i = 1;
  while ($n ** $i < 10000) {
    $i = $i + 1;
  }

  return $i;
}

echo potenciaMenorQue10000(5);
echo "<br>";
echo ceil(log(10000, 5));
echo "<br>";
echo potenciaMenorQue10000(150);
echo "<br>";
echo ceil(log(10000, 150));
echo "<br>";
