
<?php

$colores = ['blanco', 'verde', 'rojo'];


function pintarLista($l)
{
  /*   $res1 = $l[0];
  for ($i = 1; $i < count($l); $i++) {
    $res1 = $res1 . ', ' . $l[$i];
  } */

  $res1 = implode(', ', $l);

  sort($l);
  $res2 = "<ul>";
  foreach ($l as $e) {
    $res2 = $res2 . "<li>$e</li>";
  }
  $res2 = $res2 . "</ul>";

  echo $res1;
  echo "<br>";
  echo $res2;
}

pintarLista($colores);
