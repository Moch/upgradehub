<?php

function esPalindromo($n)
{
  $s = strval($n);

  return $s == strrev($s);
}

if (esPalindromo(1234321)) {
  echo "1234321 es palíndromo";
}

echo "<br>";

if (esPalindromo(2345)) {
  echo "2345 es palíndromo";
} else {
  echo "2345 no es palíndromo";
}
