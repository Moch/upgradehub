<?php

$cadenas = ["te", "patata", "cebolla", "pimienta", "agua"];

$corta = $cadenas[array_key_first($cadenas)];
$larga = $cadenas[array_key_first($cadenas)];

foreach ($cadenas as $c) {
  if (strlen($c) <= strlen($corta)) {
    $corta = $c;
  }
  if (strlen($c) >= strlen($larga)) {
    $larga = $c;
  }
}

echo $corta;
echo "<br>";
echo $larga;
