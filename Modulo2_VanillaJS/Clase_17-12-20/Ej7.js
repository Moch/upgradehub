

const streamers =
  [ { name: "Rubius"
    , age: 32
    , gameMorePlayed: "Minecraft"
    }
  , { name: "Ibai"
    , age: 25
    , gameMorePlayed: "League of Legends"
    }
  , { name: "Reven"
    , age: 43
    , gameMorePlayed: "League of Legends"
    }
  , { name: "AuronPlay"
    , age: 33
    , gameMorePlayed: "Among Us"
    }
  ]

function handleChange(event) {
  let aux = event.target.value.toLowerCase();
  let res = streamers.filter(s => s.name.toLowerCase().includes(aux))
  let node = document.createElement("p");
  node.innerText = JSON.stringify(res);
  document.body.appendChild(node);
}

const input$$ = document.querySelector("[data-function='toFilterStreamers']");

input$$.addEventListener("input", handleChange)