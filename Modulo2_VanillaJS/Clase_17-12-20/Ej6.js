

const streamers =
  [ { name: "Rubius"
    , age: 32
    , gameMorePlayed: "Minecraft"
    }
  , { name: "Ibai"
    , age: 25
    , gameMorePlayed: "League of Legends"
    }
  , { name: "Reven"
    , age: 43
    , gameMorePlayed: "League of Legends"
    }
  , { name: "AuronPlay"
    , age: 33
    , gameMorePlayed: "Among Us"
    }
  ]
  

let res = streamers.filter(s => s.gameMorePlayed.includes('Legends')); 

function f(streamer) {
  let caps = streamer.gameMorePlayed.toUpperCase();
  let res = streamer
  res.gameMorePlayed = caps;
  return res
}

let res2 = res.map(s => s.age > 35 ? f(s) : s) 

console.log(res);
console.log(res2);

// Se puede hacer solo con filter porque se modifica el objeto antes del return

const filteredLoleros = streamers.filter(streamer => {​​
  const toReturn = streamer.gameMorePlayed.toLowerCase().includes('legends');
  if(toReturn && streamer.age > 35){​​
      streamer.gameMorePlayed = streamer.gameMorePlayed.toUpperCase();
  }​​
  return toReturn;

}​​)