

const streamers =
[ { name: "Rubius"
  , age: 32
  , gameMorePlayed: "Minecraft"
  }
, { name: "Ibai"
  , age: 25
  , gameMorePlayed: "League of Legends"
  }
, { name: "Reven"
  , age: 43
  , gameMorePlayed: "League of Legends"
  }
, { name: "AuronPlay"
  , age: 33
  , gameMorePlayed: "Among Us"
  }
]

function handleChange(event) {
  const input$$ = document.querySelector("[data-function='toFilterStreamers']");
  // const input$$ = event.target.previousElementSibling;
  let aux = input$$.value.toLowerCase();
  let res = streamers.filter(s => s.name.toLowerCase().includes(aux))
  let node = document.createElement("p");
  node.innerText = JSON.stringify(res);
  document.body.appendChild(node);
}

const button$$ = document.querySelector("[data-function='toShowFilterStreamers']");

button$$.addEventListener("click", handleChange)