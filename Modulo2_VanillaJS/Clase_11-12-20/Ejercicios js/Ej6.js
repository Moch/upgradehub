const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

const list = document.createElement('ul');

for (let i = 0; i < apps.length; i++) {
  const x = document.createElement('li');
  x.textContent = apps[i];
  list.appendChild(x);
}

document.body.appendChild(list);