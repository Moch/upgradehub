Dado el siguiente array, haz una suma de todos las notas de los examenes de los alumnos usando la función .reduce().

```js

const exam = [{​​name: 'Abel Cabeza Román', score: 5}​​, {​​name: 'Maria Aranda Jimenez', score: 10}​​, {​​name: 'Cristóbal Martínez Lorenzo', score: 6}​​, {​​name: 'Mercedez Regrera Brito', score: 7}​​];

```
