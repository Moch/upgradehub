

const videogames =
  [ { name: "Final Fantasy VII"
    , gender: ["RPG"]
    , score: 9.5
    }
  , { name: "Assasins Creed Valhala"
    , gender: ["Aventura", "RPG"]
    , score: 4.5
    }
  , { name: "The last of Us 2"
    , gender: ["Acción", "Aventura"]
    , score: 9.8
    }
  , { name: "Super Mario Bros"
    , gender: ["Plataforma"]
    , score: 8.5
    }
  , { name: "Genshin Impact"
    , gender: ["RPG", "Aventura"]
    , score: 7.5
    }
  , { name: "Legend of Zelda: Breath of the wild"
    , gender: ["RPG"]
    , score: 10
    }
  ]


const RPGs = videogames.filter(game => undefined !== game.gender.find(x => x === "RPG"));

const par = RPGs.reduce((acc, game) => [acc[0] + game.score, acc[1] + 1], [0,0]);

const media = par[0] / par [1]

console.log(media);