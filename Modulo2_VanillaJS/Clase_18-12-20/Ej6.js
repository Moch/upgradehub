

const exams =
[ { name: "Abel Cabeza Román"
  , score: 5
  }
, { name: "Maria Aranda Jimenez"
  , score: 1
  }
, { name: "Cristóbal Martínez Lorenzo"
  , score: 6
  }
, { name: "Mercedes Reguera Brito"
  , score: 7
  }
, { name: "Pamela Anderson"
  , score: 3
  }
, { name: "Enrique Perez Lijó"
  , score: 6
  }
, { name: "Pedro Benitez Pacheco"
  , score: 8
  }
, { name: "Ayumi Hamasaki"
  , score: 4
  }
, { name: "Robert Kiyosaki"
  , score: 2
  }
, { name: "Keanu Reeves"
  , score: 10
  }
]


const parSumaTotal = 
  exams.reduce((acc,e) => [acc[0] + e.score, acc[1] + 1], [0,0]);

const media = parSumaTotal[0] / parSumaTotal[1];

console.log(media);