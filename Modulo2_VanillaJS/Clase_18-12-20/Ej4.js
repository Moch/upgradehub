

const exams =
  [ { name: "Abel Cabeza Román"
    , score: 5
    }
  , { name: "Maria Aranda Jimenez"
    , score: 10
    }
  , { name: "Cristóbal Martínez Lorenzo"
    , score: 6
    }
  , { name: "Mercedes Reguera Brito"
    , score: 7
    }
  ]

const total = exams.reduce((acc,e) => acc + e.score , 0);

console.log(total);