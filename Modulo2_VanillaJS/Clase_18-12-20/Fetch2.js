
const baseUrl = "https://api.nationalize.io";

const input$$ = document.querySelector('input');
const btn$$ = document.querySelector('button');


function printAPI(event) {
  const url = baseUrl + "?name=" + input$$.value;
  fetch(url).then(x => x.json()).then(x => console.log(x.country));
}; 

btn$$.addEventListener("click", printAPI);