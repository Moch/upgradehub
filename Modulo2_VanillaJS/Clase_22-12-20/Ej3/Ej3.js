

fetch("http://localhost:3000/orders").then(res => res.json()).then(res => printOrders(orderByDates(res)));


const orderByDates = (list) => {
  return list.sort((a,b) => new Date(a.date) - new Date(b.date))
}

function printOrders(orders) {
  for (const o of orders) {
    const div$ = document.createElement("div");
    const time$ = document.createElement("time");
    time$.textContent = o.date;
    div$.appendChild(time$);
    for (const p of o.products) {
      const quantity$ = document.createElement("p");
      quantity$.textContent = "Cantidad: " + p.quantity;
      const productName$ = document.createElement("p");
      fetch(`http://localhost:3000/products/${p.productId}`).then(res => res.json()).then(res => productName$.textContent = res.name);
      div$.appendChild(productName$);
      div$.appendChild(quantity$);
    }
    document.body.appendChild(div$);
    document.body.appendChild(document.createElement("hr"));
  }
};