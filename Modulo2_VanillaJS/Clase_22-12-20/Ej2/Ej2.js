
const baseUrl = "http://localhost:3000/diary";

fetch(baseUrl).then(res => res.json()).then(res => display(orderDiaryNotes(res)));

const orderDiaryNotes = (diary) => {
  return diary.sort((a,b) => new Date(a.date) - new Date(b.date))
}

function display(res) {
  for (const e of res) {
    const div$ = document.createElement("div");
    const h3$ = document.createElement("h3");
    const time$ = document.createElement("time");
    const p$ = document.createElement("p");
    const btn$ = document.createElement("button");

    h3$.textContent = e.title;
    time$.textContent = e.date;
    p$.textContent = e.description;
    btn$.textContent = "Eliminar";
    btn$.addEventListener("click", () => btn$.parentNode.remove())

    div$.appendChild(h3$);
    div$.appendChild(time$);
    div$.appendChild(p$);
    div$.appendChild(btn$);

    document.body.appendChild(div$);
  }
}