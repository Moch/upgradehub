

fetch("http://localhost:3000/planets").then(res => res.json()).then(res => printPlanets(res));

function printPlanets(planets) {
  for (const p of planets) {
    const col$ = document.createElement("div");
    col$.innerHTML = `<figure><img src='${p.image}'><figcaption>${p.name}</figcaption></figure>`;
    col$.addEventListener("click", () => 
      fetch("http://localhost:3000/characters?idPlanet=" + p.id).then(res => res.json()).then(chars => {
        for (const c of chars) {
          const div$ = document.createElement("div");
          div$.classList.add("character");
          const fig$ = document.createElement("figure");
          fig$.innerHTML = `<img src='${c.avatar}'><figcaption>${c.name}</figcaption>`
          const p$ = document.createElement("p");
          p$.textContent = c.description;
          const hr$ = document.createElement("hr");
          div$.appendChild(fig$);
          div$.appendChild(p$);
          div$.appendChild(hr$);
          col$.appendChild(div$);
        };
      }));
    document.body.appendChild(col$);
  }
}