

fetch("https://breakingbadapi.com/api/characters").then(res => res.json()).then(res => displayAPI(res));


function displayAPI(res) {
  for (const c of res) {
    const fig = document.createElement("figure");
    const image = document.createElement("img");
    const caption = document.createElement("figcaption");
    image.setAttribute("src", c.img);
    caption.textContent = c.name;
    fig.appendChild(image);
    fig.appendChild(caption);
    document.body.appendChild(fig);
  }
}