
const cities =
  [ {isVisited: true  , name: "Tokyo"}
  , {isVisited: false , name: "Madagascar"}
  , {isVisited: true  , name: "Amsterdam"}
  , {isVisited: false , name: "Seoul"}
  ]

function f(city) {
  if (city.isVisited) {
    return city.name + " (Visitado)"
  } else {
    return city.name
  }
}

const cityNames = cities.map(f);

console.log(cityNames);
