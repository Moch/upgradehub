

const users = 
  [ {id: 1, name: "Abel"}
  , {id: 2, name: "Julia"}
  , {id: 3, name: "Pedro"}
  , {id: 4, name: "Amanda"}
  ]

function f(u) {
  let res = "";
  if (u.name[0] === 'A') {
    res = "Anacleto"
  } else {
    res = u.name
  }
  return res;
}

const names = users.map(f);

console.log(names);