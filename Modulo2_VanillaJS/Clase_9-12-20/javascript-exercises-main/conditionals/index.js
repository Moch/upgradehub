const number1 = 10;
const number2 = 20;
const number3 = 2;

// ejemplo
if(number1 === 10){
    console.log('number1 is strictly equal to 10')
}

if (number2 / number1 === 2) {
  console.log("number2 divided by number1 is equal to 2");
}

if (number1 !== number2) {
  console.log("number1 is strictly different to number2");
}

if (number3 != number1) {
  console.log("number3 is different to number1");
}

if (number3 * 5 === number1) {
  console.log("number3 mutiplied by 5 is equal to number1");
}

if (number3 * 5 === number1 && number1 * 2 == number2) {
  console.log("number3 multiplied by 5 is equal to number1 AND number1 multiplied by 2 is equal to number2");
}

if (number2 / 2 === number1 || number1 / 5 === number3) {
  console.log("number2 divided by 2 is equal to number1 OR number1 divided by 5 is equal to number3");
}

