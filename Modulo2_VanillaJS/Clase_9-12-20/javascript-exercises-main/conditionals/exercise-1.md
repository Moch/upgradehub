# English
In base to following code, show correct console messages.



```js
const number1 = 10;
const number2 = 20;
const number3 = 2;

// ejemplo
if(number1 === 10){
    console.log('number1 is strictly equal to 10')
}

if (/* COMPLETAR */) {
  console.log("number2 divided by number1 is equal to 2");
}

if (/* COMPLETAR */) {
  console.log("number1 is strictly different to number2");
}

if (/* COMPLETAR */) {
  console.log("number3 is different to number1");
}

if (/* COMPLETAR */) {
  console.log("number3 mutiplied by 5 is equal to number1");
}

if (/* COMPLETAR */) {
  console.log("number3 multiplied by 5 is equal to number1 AND number1 multiplied by 2 is equal to number2");
}

if (/* COMPLETAR */) {
  console.log("number2 divided by 2 is equal to number1 OR number1 divided by 5 is equal to number3");
}

```

# Español
En base al código siguiente, muestra los mensajes correctos por consola.

```js
const number1 = 10;
const number2 = 20;
const number3 = 2;

// ejemplo
if(number1 === 10){
    console.log('number1 es estrictamente igual a 10')
}

if (/* COMPLETAR */) {
  console.log("number2 dividido entre number1 es igual a 2");
}

if (/* COMPLETAR */) {
  console.log("number1 es estrictamente distinto a number2");
}

if (/* COMPLETAR */) {
  console.log("number3 es distinto number1");
}

if (/* COMPLETAR */) {
  console.log("number3 por 5 es igual a number1");
}

if (/* COMPLETAR */) {
  console.log("number3 por 5 es igual a number1 Y number1 por 2 es igual a number2");
}

if (/* COMPLETAR */) {
  console.log("number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3");
}

```