# English
Use the correct assignment operator that will result in x = 15, having two variables y = 10 and z = 5.

# Español
Usa el correcto operador de asignación que resultará en x = 15, teniendo dos variables y = 10 y z = 5.