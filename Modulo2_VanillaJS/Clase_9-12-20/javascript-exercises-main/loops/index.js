
var i;

for (i = 0; i < 10; i++) {
  console.log(i);
}

console.log("-------------------------------");

for (i = 0; i < 10; i++) {
  if (i % 2 === 0) {
    console.log(i);
  }
}

console.log("-------------------------------");

for (i = 0; i < 10; i++) {
  console.log("Intentando dormir");
}
console.log("Dormido!");