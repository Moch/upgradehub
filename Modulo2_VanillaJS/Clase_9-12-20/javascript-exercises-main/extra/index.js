const users = 
  [ {name: "Abel", years: 43}
  , {name: "Maria", years: 18}
  , {name: "Pedro", years: 14}
  , {name: "Samantha", years: 32}
  , {name: "Raquel", years: 16}
  ];

let menores = [];
let mayores = [];

let i = 0;

for (i = 0; i < users.length; i++) {
  if (users[i].years < 18) {
    menores.push(users[i].name)
  }
  else {
    mayores.push(users[i].name)
  }
}

console.log("Usuarios menores de edad: ")
console.log(menores.join(", "))
console.log("Usuarios mayores de edad: ")
console.log(mayores.join(", "))

console.log("----------------------------------")

const fruits = 
  [ 'Strawberry'
  , 'Banana'
  , 'Orange'
  , 'Apple'
  ];

const foodSchedule = 
  [ {name: "Salad", isVegan: true}
  , {name: "Salmon", isVegan: false}
  , {name: "Tofu", isVegan: true}
  , {name: "Burger", isVegan: false}
  , {name: "Rice", isVegan: true}
  , {name: "Pasta", isVegan: true}
  ];

let onlyVegan = [];


for (i = 0; i < foodSchedule.length; i++) {
  if (foodSchedule[i].isVegan) {
    onlyVegan.push(foodSchedule[i].name)
  }
  else {
    onlyVegan.push(fruits.pop())
  }
}

console.log(onlyVegan.join(", "))

console.log("----------------------------------")

const movies = 
  [ {name: "Your Name", durationInMinutes: 130}
  , {name: "Pesadilla antes de navidad", durationInMinutes: 225}
  , {name: "Origen", durationInMinutes: 165}
  , {name: "El señor de los anillos", durationInMinutes: 967}
  , {name: "Solo en casa", durationInMinutes: 214}
  , {name: "El jardin de las palabras", durationInMinutes: 40}
  ];


let shortMovies = [];
let mediumMovies = [];
let longMovies = [];

for (i = 0; i < movies.length; i++) {
  if (movies[i].durationInMinutes < 100) {
    shortMovies.push(movies[i].name)
  } else if (movies[i].durationInMinutes >= 200) {
    longMovies.push(movies[i].name)
  } else {
    mediumMovies.push(movies[i].name)
  }
}

console.log(shortMovies.join(", "))
console.log(mediumMovies.join(", "))
console.log(longMovies.join(", "))

console.log("----------------------------------")