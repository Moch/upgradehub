# English
Alert the number of items in an array, using the correct Array property.

```js
const cars = ["Saab", "Volvo", "BMW"];
```

# Español
Alerta el numero de elementos en el array usando la propiedad correcto de Array.

```js
const cars = ["Saab", "Volvo", "BMW"];
```