
const cars = ["Saab", "Volvo", "BMW"];

console.log(cars[1]);

cars[0] = "Ford";

alert(cars.length);

const RickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
RickAndMortyCharacters.push("Morty");
RickAndMortyCharacters.push("Summer");

console.log(RickAndMortyCharacters[RickAndMortyCharacters.length - 1]);

const RickAndMortyCharacters2 = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];

RickAndMortyCharacters2.pop();

console.log(RickAndMortyCharacters2[0] + " y " + RickAndMortyCharacters[3]);