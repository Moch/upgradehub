

const datos = 
  {​​ categorias: 
    [ {​​ text: "Personajes"
      , path: "/personajes"
      , url: "https://swapi.dev/api/people/"
      }​​
    , { text: "Planetas"
      , path: "/planetas"
      , url: "https://swapi.dev/api/planets/"
      }
    ​​]
  }​​



const main = document.querySelector("main");


function crearEnlace(linkText, f) {
  const a = document.createElement("a");
  a.textContent = linkText;
  a.setAttribute("href", "#");
  a.addEventListener("click", function(e) {
    e.preventDefault();
    f();
  }
  )
  return a;
}


function cabecera() {
  const cabecera = document.createElement("header");
  const imagenCabecera = document.createElement("img");
  imagenCabecera.src = "./assets/img/Star_Wars_Logo.svg"
  cabecera.appendChild(imagenCabecera);

  main.appendChild(cabecera);
}


function navegador() {
  const nav = document.createElement("nav");
  main.appendChild(nav);

  for (let i = 0; i < datos.categorias.length; i++) {
    const enlace = crearEnlace(datos.categorias[i].text, function() {
      console.log("Hola");
    }
    )
    
  }
}



window.onload = () => {
  cabecera();
  navegador();
}