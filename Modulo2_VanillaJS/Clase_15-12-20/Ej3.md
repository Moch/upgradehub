# Español

Usa el siguiente código como base y crea 3 funciones llamadas father, confirmExample, promptExample. La función confirmExample recibirá una variable de texto que mostrará como titulo de la ventana y retornará el valor del confirm. La función promptExample hará lo mismo pero con un prompt. La función father recibirá como parámetros tex, con el valor del titulo de las ventanas y una función callback (confirmExample o promptExample).

La función father deberá ejecutar la función que reciba como callback y añadir el valor resultado que retorne la función al array numersList.

Ejecuta varias veces la función father y haz finalmente un console.log de userAnwsers

```js

const userAnwsers = [];



function confirmExample(){​​



}​​



function promptExample(){​​



}​​



function father(){​​



}​​

```
