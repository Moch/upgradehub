

const userAnswers = [];


function confirmExample(t) {
  return confirm(t);
}


function promptExample(t) {
  return prompt(t);
}


function father(t,f) {
  userAnswers.push(f(t));
}


for (let i = 0; i < 8; i++) {
  if (i % 2 === 0) {
    father(i, confirmExample)
  }
  else {
    father(i, promptExample)
  } 
}

console.log(userAnswers);