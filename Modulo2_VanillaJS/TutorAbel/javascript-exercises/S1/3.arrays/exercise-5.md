# English
Remove last element of array and show first and fourth element in a console message.
```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
```

# Español
Elimina el último elemento del array y muestra el primero y el cuarto por consola.
```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
```