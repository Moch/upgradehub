# English
Get the value "Volvo" from the cars array and show it in console.

```js
const cars = ["Saab", "Volvo", "BMW"];
```

# Español
Consigue el valor "Volvo" del array de cars y muestralo por consola.

```js
const cars = ["Saab", "Volvo", "BMW"];
```