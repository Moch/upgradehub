# English
Remove second element of array and show array in a console message.
```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
```

# Español
Elimina el segundo elemento del array y muestra el array por consola.
```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
```