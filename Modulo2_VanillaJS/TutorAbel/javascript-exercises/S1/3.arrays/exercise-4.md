# English
Add 2 elements to the array: "Morty" and "Summer". Show in console the last character of array

```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
```

# Español
Añade 2 elementos al array: "Morty" y "Summer". Muestra en consola el último personaje del array
```js
const RickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
```