# English
Create a loop that runs from 0 to 9 and show a console message only when remainder of number divided by 2 is 0.

# Español
Crea un bucle for que vaya desde 0 a 9 y muestralo por consola solo cuando el resto del numero dividido entre 2 sea 0.