# English
Create a loop to get to sleep counting sheeps. This loop start in 0 and finish in 10. Show a console message 'Trying to sleep' and change the message in last loop to 'Asleep!'.

# Español
Crea un bucle para conseguir dormir contando ovejas. Este bucle empieza en 0 y termina en 10. Muestra por consola un mensaje diciendo 'Intentando dormir' en cada vuelta del bucle y cambia el mensaje en la última vuelta a 'Dormido!'.